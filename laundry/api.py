"""API access."""
import base64
import json
import urllib.request
import typing

import jwt

from . import config

with open('secret.key') as _f:
    _key = _f.read()

_auth = base64.encodebytes(
    f"{config.clientid}:{config.clientsecret}".encode()).decode()
_headers = {"Authorization": "Basic " + _auth.rstrip(),
            "Content-Type": "application/json"}


class Response(typing.TypedDict):
    status: str


class MachineStatusData(typing.TypedDict):
    online: bool
    running: bool
    startTime: str
    endTime: str
    type: str
    machineName: str
    price: typing.Dict[str, str]


class MachineStatus(typing.TypedDict, Response):
    data: typing.Dict[str, MachineStatusData]


def _request(path: str, payload: typing.Mapping[str, object]) -> typing.Any:
    """Laundro API helper."""
    token = jwt.encode(payload, _key, algorithm='RS256').decode()
    body = json.dumps(dict(token=token)).encode()
    req = urllib.request.Request(config.endpoint + path, body, _headers)
    with urllib.request.urlopen(req) as f:
        response = json.loads(f.read().decode())
        if response['status'] != 'Success':
            raise ValueError(response['status'])
        return response


def status() -> MachineStatus:
    """Laundro API to list machine status."""
    payload = dict(opId=config.opid, outletId=config.outletid)
    return _request('machineStatus', payload)['data']


def pay(amount: float, machine: str) -> Response:
    """Laundro API to pay machine."""
    payload = dict(opId=config.opid, outletId=config.outletid,
                   terminalId=config.terminalid, method="APCard",
                   amount=amount, transId="1", mchNo=machine)
    return _request('terminalPay', payload)
